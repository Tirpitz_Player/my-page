from django.contrib.auth.models import User
from faker import Faker


class TextMixin:
    """ TestMixin """
    faker = Faker()

    def get_or_create_user(self):
        """ Get or create new user """
        profile = self.faker.simple_profile()
        user, _ = User.objects.get_or_create(username=profile['username'], password=profile['username'])
        return user

from django.contrib.auth.models import User
from django.db import models
from django.core.validators import MinLengthValidator
from django.utils.text import slugify
from django.utils.translation import gettext_lazy as _


def get_or_create_delete():
    """ Get deleted user """
    return User.objects.get_or_create(name='Erased')[0]


class Category(models.Model):
    """ Forum category model """
    name = models.CharField(_('Category name'), max_length=64, validators=[MinLengthValidator(3)])
    order_number = models.PositiveSmallIntegerField(_('Order number'), default=0)

    class Meta:
        """ Meta class """
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')
        ordering = ['-order_number']

    def __str__(self):
        return self.name


class Topic(models.Model):
    """ Topic model """
    name = models.CharField(_('Topic'), max_length=128, validators=[MinLengthValidator(12)])
    created_at = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(_('Active'), default=True)
    category = models.ForeignKey(Category, on_delete=models.PROTECT, blank=False, null=True)

    class Meta:
        """ Meta class """
        verbose_name = _('Topic')
        verbose_name_plural = _('Topics')
        ordering = ['-created_at']

    def __str__(self):
        return slugify(self.name)


class Post(models.Model):
    """ Post class """
    text = models.TextField(_('Text'))
    created_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, blank=False, null=False)
    author = models.ForeignKey(User, null=True, blank=False, on_delete=models.SET(get_or_create_delete))

    class Meta:
        """ Meta class """
        verbose_name = _('Post')
        verbose_name_plural = _('Posts')
        ordering = ['created_at']

from django.test import TestCase, Client
from django.urls import reverse_lazy
from .test_mixins import TextMixin


# Create your tests here.
class IndexViewTests(TextMixin, TestCase):
    """"""
    def setUp(self) -> None:
        """ Override setUp method """
        super(IndexViewTests, self).setUp()
        self.client = Client()

    def test_index_view_is_loaded(self):
        """ Test loading index page """
        response = self.client.get(reverse_lazy('core:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'Python Academy Forum')

    def test_index_vew_after_login(self):
        """ Test loading index page after login """
        user = self.get_or_create_user()
        self.client.force_login(user)

        response = self.client.get(reverse_lazy('core:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, f'{user.username}')
        self.assertContains(response, 'Logout')
        self.assertContains(response, 'Change password')
        self.assertNotContains(response, 'Login')

    def text_index_without_categories(self):
        """ Test index page without categories """
        response = self.client.get(reverse_lazy('core:index'))
        self.assertEqual(200, response.status_code)
        self.assertContains(response, 'No categories')

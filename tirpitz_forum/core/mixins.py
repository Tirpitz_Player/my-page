from django.contrib.auth.mixins import UserPassesTestMixin


class SuperUserMixin(UserPassesTestMixin):
    """ Mixin allow to check user is superuser """

    def test_func(self):
        return self.request.user.is_superuser

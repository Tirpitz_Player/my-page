from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.index_view, name='index'),
    path('create/category', views.create_category_view, name='create_category'),
    path('delete/category/<int:pk>', views.delete_category_view, name='delete_category'),
    path('update/category/<int:pk>', views.update_category_view, name='update_category'),
    path('topics/<int:pk>', views.topics_list_view, name='topics'),
    path('register', views.register_view, name='register'),
    path('create/topic/<int:pk>', views.topic_create_view, name='create_topic'),
    path('topics/posts/<int:pk>', views.topics_posts_view, name='topics_posts'),
    path('create/posts/<int:pk>', views.create_post_view, name='create_post'),
]

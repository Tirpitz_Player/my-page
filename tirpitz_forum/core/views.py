from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.db import transaction
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import CreateView, FormView, ListView, DeleteView, UpdateView, DetailView
from django.utils.translation import gettext_lazy as _
from django.urls import reverse_lazy
from .forms import CreateCategoryForm, RegisterForm, CreateTopicForm, CreatePostForm
from .models import Category, Topic, Post
from .mixins import SuperUserMixin


class IndexView(ListView):
    """ Index View """
    model = Category
    template_name = "index.html"


class CreateCategoryView(LoginRequiredMixin, SuperUserMixin, SuccessMessageMixin, CreateView):
    """ Add Category View """
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')
    success_message = _("Category successfully created")


class DeleteCategoryView(LoginRequiredMixin, SuperUserMixin, SuccessMessageMixin, DeleteView):
    """ Delete Category View """
    model = Category
    template_name = "delete_category.html"
    success_url = reverse_lazy('core:index')
    success_message = _("Category successfully deleted")


class UpdateCategoryView(LoginRequiredMixin, SuperUserMixin, SuccessMessageMixin, UpdateView):
    """ Update Category View """
    model = Category
    form_class = CreateCategoryForm
    template_name = "create_category.html"
    success_url = reverse_lazy('core:index')
    success_message = _("Category successfully updated")


class TopicsListView(ListView):
    """ Category List View """
    model = Topic
    template_name = 'topics_list.html'
    paginate_by = 20

    def get_queryset(self):
        """ Override queryset"""
        category_pk = self.kwargs['pk']
        return Topic.objects.filter(category_id=category_pk)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Override get context data """
        context = super(TopicsListView, self).get_context_data(object_list=object_list, **kwargs)
        context['category'] = Category.objects.get(id=self.kwargs['pk'])
        return context


class TopicCreateView(LoginRequiredMixin, SuccessMessageMixin, FormView):
    """ Topic Create View """
    model = Topic
    form_class = CreateTopicForm
    model_2 = Post
    form_class_2 = CreatePostForm
    template_name = 'create_topic.html'
    success_message = _('Topic created with success')
    success_url = reverse_lazy('core:index')

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Override get context data """
        context = super(TopicCreateView, self).get_context_data(object_list=object_list, **kwargs)
        context['form2'] = self.form_class_2()
        return context

    def post(self, request, *args, **kwargs):
        """ Override post method """
        response = super(TopicCreateView, self).post(request, *args, **kwargs)
        form = self.form_class({'name': request.POST['name']})
        form2 = self.form_class_2({'text': request.POST['text']})

        if form.is_valid() and form2.is_valid():
            with transaction.atomic():
                new_topic = self.model(name=request.POST['name'], category_id=self.kwargs['pk'])
                new_topic.save()

                first_post = self.model_2(text=request.POST['text'], topic=new_topic, author=request.user)
                first_post.save()

            return redirect(self.success_url)

        return render(request, self.template_name, {'form': form, 'form2': form2})


class RegisterView(CreateView):
    """ Register View """
    template_name = 'registration/register.html'
    form_class = RegisterForm
    success_url = reverse_lazy('core:index')
    success_message = _("User successfully created")


class TopicPostsView(ListView):
    """ List of posts from topic View """
    model = Post
    template_name = 'topics_posts.html'
    form_class = CreatePostForm
    paginate_by = 20

    def get_queryset(self):
        """ Override queryset"""
        topic_pk = self.kwargs['pk']
        return Post.objects.filter(topic_id=topic_pk)

    def get_context_data(self, *, object_list=None, **kwargs):
        """ Override get context data """
        context = super(TopicPostsView, self).get_context_data(object_list=object_list, **kwargs)
        context['topic'] = Topic.objects.get(id=self.kwargs['pk'])
        context['form'] = self.form_class
        return context


class CreatePostView(LoginRequiredMixin, CreateView):
    """ Create new post by authenticated user View """
    model = Post
    form_class = CreatePostForm
    success_message = _("Post successfully created")

    def get(self, *args, **kwargs):
        """ Override get method """
        return redirect(reverse_lazy('core:topics_posts', kwargs=kwargs))

    def post(self, request, *args, **kwargs):
        """ Override post method """
        form = self.form_class({'text': request.POST['text']})

        if not form.is_valid():
            messages.error(request, _("Incorrect data in post from"))
            return redirect(reverse_lazy('core:topics_posts', kwargs=kwargs))

        post = self.model(text=request.POST['text'], topic_id=kwargs['pk'], author=request.user)
        post.save()

        messages.success(request, self.success_message)
        return redirect(reverse_lazy('core:topics_posts', kwargs=kwargs))


# Create views
index_view = IndexView.as_view()
register_view = RegisterView.as_view()
topics_list_view = TopicsListView.as_view()
topic_create_view = TopicCreateView.as_view()
create_category_view = CreateCategoryView.as_view()
delete_category_view = DeleteCategoryView.as_view()
update_category_view = UpdateCategoryView.as_view()
topics_posts_view = TopicPostsView.as_view()
create_post_view = CreatePostView.as_view()

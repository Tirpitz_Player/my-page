""" Forms """
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.forms import UserCreationForm
from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Category, Topic, Post


class CreateCategoryForm(ModelForm):
    """ Create category form """

    class Meta:
        """ Meta class """
        model = Category
        fields = ['name', 'order_number']


class CreateTopicForm(ModelForm):
    """ Create topic form """

    class Meta:
        """ Meta class """
        model = Topic
        fields = ['name']


class CreatePostForm(ModelForm):
    """ Create post form """

    class Meta:
        """ Meta class """
        model = Post
        fields = ['text']


class RegisterForm(UserCreationForm):
    """ Register user form """

    first_name = forms.CharField(max_length=20, required=False, help_text=_("Optional"))
    last_name = forms.CharField(max_length=30, required=False, help_text=_("Optional"))
    email = forms.EmailField(max_length=128, required=True, help_text=_("Required"))

    class Meta:
        """ Meta class """
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']
